@extends('app')

@section('content')


        <div id="pcont" class="container-fluid">

            <div class="page-head">
                <h2>Form Elements</h2>
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Forms</a></li>
                    <li class="active">Elements </li>
                </ol>
            </div>

            <div class="cl-mcont">
                <div class="row">
                    <div class="col-md-12">
                        <div class="block-flat">
                            <div class="header">
                                <div class="pull-right"><button data-modal="colored-danger" class="btn btn-danger btn-flat md-trigger">Delete</button></div>
                                <h3>Create Post</h3>
                            </div>
                            <div class="content">
                                <h1>{{$data->title}}</h1>
                                <h1>{{$data->body}}</h1>
                                <h1><small>{{$data->created_at}}</small></h1>

                            </div>
                        </div>
                    </div>
                </div>

                <!-- Nifty Modal-->
                <div id="colored-danger" class="md-modal colored-header danger md-effect-10">
                    <div class="md-content">
                        <div class="modal-header">
                            <h3>คุณต้องการที่จะลบ ?</h3>
                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="text-center">
                                <div class="i-circle danger"><i class="fa fa-check"></i></div>
                                <form method="POST" action="/workshop/{{$data->id}}" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <input name="_method" type="hidden" value="DELETE">

                                    <button type="submit" data-dismiss="modal" class="btn btn-danger btn-flat md-close">Proceed</button>
                                </form>

                            </div>
                        </div>
                        <div class="modal-footer">
                            {{--<button type="button" data-dismiss="modal" class="btn btn-default btn-flat md-close">Cancel</button>--}}
                            {{--<button type="submit" data-dismiss="modal" class="btn btn-danger btn-flat md-close">Proceed</button>--}}
                        </div>

                    </div>
                </div>
                <div class="md-overlay"></div>


            </div>

        </div>

@endsection

@section('scripts')


    <script type="text/javascript">
        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.formElements();

            $('.md-trigger').modalEffects();
        });

        $('div.alert').delay(3000).slideUp(300);
    </script>

@endsection