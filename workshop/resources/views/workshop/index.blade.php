@extends('app')

@section('content')


        <div id="pcont" class="container-fluid">

            <div class="page-head">
                <h2>Form Elements</h2>
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Forms</a></li>
                    <li class="active">Elements </li>
                </ol>
            </div>

            <div class="cl-mcont">
                <div class="row">
                    <div class="col-md-12">
                        <div class="block-flat">
                            <div class="header">
                                <h3>All Post</h3>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="block-flat">
                                            <div class="header">
                                                <h3>Horizontal Icons</h3>
                                            </div>
                                            <div class="content">
                                                <div>
                                                    <table id="datatable-icons" class="table table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Title</th>
                                                            <th>Body</th>
                                                            <th>Action</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($datas as $data)

                                                        <tr class="odd gradeX">
                                                            <td>{{$data->id}}</td>
                                                            <td>
                                                                {{$data->title}}
                                                            </td>
                                                            <td>{{$data->body}}</td>
                                                            <td class="text-right color-success">
                                                                <a href="{{url('/workshop/'.$data->id.'/edit')}}"><i class="fa fa-pencil-square-o"></i></a> |
                                                                <a href="{{url('/workshop/'.$data->id)}}"><i class="fa fa-bullseye"></i></a>
                                                            </td>
                                                        </tr>
                                                        </tr>

                                                        @endforeach

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <!-- Nifty Modal-->
        <div id="colored-danger" class="md-modal colored-header danger md-effect-10">
            <div class="md-content">
                <div class="modal-header">
                    <h3>Custom Header Color</h3>
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close">×</button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <div class="i-circle danger"><i class="fa fa-check"></i></div>
                        <h4>Awesome!</h4>
                        <p>Changes has been saved successfully!</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default btn-flat md-close">Cancel</button>
                    <button type="button" data-dismiss="modal" class="btn btn-danger btn-flat md-close">Proceed</button>
                </div>
            </div>

@endsection

@section('scripts')


    <script type="text/javascript">
        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.dataTables();
        });


        $('div.alert').delay(3000).slideUp(300);
    </script>

@endsection