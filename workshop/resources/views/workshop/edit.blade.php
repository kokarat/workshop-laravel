@extends('app')

@section('content')


        <div id="pcont" class="container-fluid">

            <div class="page-head">
                <h2>Form Elements</h2>
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Forms</a></li>
                    <li class="active">Elements </li>
                </ol>
            </div>

            <div class="cl-mcont">
                <div class="row">
                    <div class="col-md-12">
                        <div class="block-flat">
                            <div class="header">
                                <h3>Create Post</h3>
                            </div>
                            <div class="content">
                                <form method="POST" action="/workshop/{{$data->id}}" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <input name="_method" type="hidden" value="PATCH">

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Title</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" name="title" value="{{$data->title}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Body</label>
                                        <div class="col-sm-6">
                                            <textarea class="form-control" name="body">{{$data->body}}</textarea>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

@endsection

@section('scripts')


    <script type="text/javascript">
        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.formElements();
        });

        $('div.alert').delay(3000).slideUp(300);
    </script>

@endsection