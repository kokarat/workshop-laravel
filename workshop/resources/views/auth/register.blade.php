<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="assets/img/favicon.png">
	<title>Laravel Workshop</title>
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Raleway:300,200,100" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/lib/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('assets/lib/font-awesome/css/font-awesome.min.css')}}">
	<!--if lt IE 9script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
    -->
	<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/jquery.nanoscroller/css/nanoscroller.css')}}">
	<link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
</head>
<body class="texture">
<div id="cl-wrapper" class="sign-up-container">
	<div class="middle-sign-up">
		<div class="block-flat">

			@include('errors.errors')

			<div class="header">
				<h3 class="text-center"><img src="{{asset('assets/img/logo.png')}}" alt="logo" class="logo-img">Laravel Workshop</h3>
			</div>
			<div>
				<form style="margin-bottom: 0px !important;" method="POST" action="/auth/register" parsley-validate="" novalidate="" class="form-horizontal">
					{!! csrf_field() !!}

					<div class="content">
						<h5 class="title text-center"><strong>Sign Up</strong></h5>
						<hr>
						<div class="form-group">
							<div class="col-sm-12">
								<div class="input-group"><span class="input-group-addon"><i class="fa fa-user"></i></span>
									<input type="text" name="name" data-parsley-trigger="change" data-parsley-errors-container="#nick-error" required="" placeholder="Username" class="form-control">
								</div>
								<div id="nick-error"></div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<div class="input-group"><span class="input-group-addon"><i class="fa fa-envelope"></i></span>
									<input type="email" name="email" data-parsley-trigger="change" data-parsley-errors-container="#email-error" required="" placeholder="E-mail" class="form-control">
								</div>
								<div id="email-error"></div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-6 col-xs-6">
								<div class="input-group"><span class="input-group-addon"><i class="fa fa-lock"></i></span>
									<input id="pass1" name="password" type="password" data-parsley-errors-container="#password-error" placeholder="Password" required="" class="form-control">
								</div>
								<div id="password-error"></div>
							</div>
							<div class="col-sm-6 col-xs-6">
								<div class="input-group"><span class="input-group-addon"><i class="fa fa-lock"></i></span>
									<input parsley-equalto="#pass1" name="password_confirmation" type="password" data-parsley-errors-container="#confirmation-error" required="" placeholder="Confirm Password" class="form-control">
								</div>
								<div id="confirmation-error"></div>
							</div>
						</div>
						<p class="spacer">By creating an account, you agree with the <a href="#">Terms</a> and <a href="#">Conditions</a>.</p>
						<button type="submit" class="btn btn-block btn-success btn-rad btn-lg">Sign Up</button>
					</div>
				</form>
			</div>
		</div>
		<div class="text-center out-links"><a href="#">© 2014 Your Company</a></div>
	</div>
</div>
<script type="text/javascript" src="{{asset('assets/lib/jquery/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/lib/jquery.nanoscroller/javascripts/jquery.nanoscroller.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/cleanzone.js')}}"></script>
<script src="{{asset('assets/lib/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/voice-recognition.js')}}"></script>
<script src="{{asset('assets/lib/jquery.parsley/dist/parsley.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/page-signup.js')}}" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		//initialize the javascript
		App.init();
		App.pageSignUp();

	});
</script>
</body>
</html>