@extends('app')

@section('content')
    <div class="container-fluid">

        <div id="cl-wrapper" class="login-container">

            <div class="middle-login">
                <div class="block-flat">
                    <div class="header">
                        <h3 class="text-center">Reset Password</h3>
                    </div>
                    <div>
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="content">
                                <h4 class="title">Enter email</h4>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                            <input type="text" name="email" placeholder="email" id="email" class="form-control">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="foot">
                                <button class="btn btn-primary" data-dismiss="modal" type="submit">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="text-center out-links"><a href="#">&copy; 2015 Hunz WebApp</a></div>
            </div>

        </div>
@endsection
