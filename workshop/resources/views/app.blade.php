<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel Workshop</title>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Raleway:300,200,100" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/lib/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/lib/font-awesome/css/font-awesome.min.css')}}">
    <!--if lt IE 9script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
    -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/lib/jquery.nanoscroller/css/nanoscroller.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/lib/bootstrap.switch/css/bootstrap3/bootstrap-switch.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/lib/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/lib/jquery.select2/select2.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/lib/bootstrap.slider/css/bootstrap-slider.cs')}}s">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/lib/bootstrap.daterangepicker/daterangepicker-bs3.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/lib/jquery.icheck/skins/square/blue.css')}}">
    <link rel="stylesheet" href="{{asset('assets/lib/font-awesome/css/font-awesome.min.css')}}">


    <link rel="stylesheet" type="text/css" href="{{asset('assets/lib/jquery.nanoscroller/css/nanoscroller.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/lib/jquery.datatables/plugins/bootstrap/3/dataTables.bootstrap.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/lib/jquery.niftymodals/css/component.css')}}">

    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

</head>
<body>




@include('partials.nav')
        <!--Sidebar item function-->
@include('partials.sidebar')
        <!--End Sidebar item function-->

<!-- Content -->
@yield('content')
        <!-- End Content -->

<!-- Scripts -->
<script type="text/javascript" src="{{asset('assets/lib/jquery/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/lib/jquery.nanoscroller/javascripts/jquery.nanoscroller.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/cleanzone.js')}}"></script>
<script src="{{asset('assets/lib/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/voice-recognition.js')}}"></script>
<script src="{{asset('assets/lib/jquery.select2/select2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/bootstrap.slider/js/bootstrap-slider.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery.nestable/jquery.nestable.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/bootstrap.switch/js/bootstrap-switch.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery.icheck/icheck.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/bootstrap.daterangepicker/daterangepicker.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/bootstrap.slider/js/bootstrap-slider.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/js/page-form-elements.js')}}"></script>

<script src="{{asset('assets/lib/jquery.datatables/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery.datatables/plugins/bootstrap/3/dataTables.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/page-data-tables.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery.niftymodals/js/jquery.modalEffects.js')}}" type="text/javascript"></script>

@include('partials.flash')

@yield('scripts')

        <!-- End Scripts -->
</body>
</html>
