<?php

    namespace App\Http\Controllers;

    use App\Workshop;
    use Illuminate\Http\Request;
    use App\Http\Requests;
    use App\Http\Controllers\Controller;

    class WorkshopController extends Controller
    {

        /**
         * Need to authentication
         */
        public function __construct()
        {
            $this->middleware('auth');
        }

        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $datas = Workshop::all();
            return view('workshop.index',compact('datas'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('workshop.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {




            $file_path = $request->file('file_name')->move('uploads', $request->file('file_name')->getClientOriginalName());

            /**
             * Insert table 1
             */
            $workshop = Workshop::create([
                'title'    => $request->get('title'),
                'body'    => $request->get('body'),
                'file_path'    => $file_path,
            ]);

            /**
             * Insert table 2
             */
//            $workshop = Workshop::create([
//                'title'    => $request->get('title'),
//                'body'    => $request->get('body'),
//                'file_path'    => $file_path,
//            ]);



            return redirect('workshop/'.$workshop->id);

        }

        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            $data = Workshop::find($id);

            return view('workshop.show',compact('data'));
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $data = Workshop::find($id);

            return view('workshop.edit',compact('data'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $data = Workshop::find($id);

            $data->title  = $request->get('title');
            $data->body  = $request->get('body');

            $data->save();

            return redirect('workshop/'.$id);
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $data = Workshop::find($id);

            $data->delete();

            return redirect('workshop');
        }


        public function read_csv(){

            $file = fopen(public_path().'/uploads/demo.csv', 'r');

            while (($line = fgetcsv($file)) !== FALSE) {

                //print_r($line);

                Workshop::create([
                    'title' => $line[0],
                    'body'  => $line[1]
                ]);

            }
            fclose($file);
        }
    }
