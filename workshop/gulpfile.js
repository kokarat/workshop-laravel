var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

 mix.sass('app.scss','resources/assets/css/app.css');

 /**
  *  CSS
  */
 mix.styles([
  'bootstrap.css',
  'style.css',
  'app.css',
 ]);

 /**
  * Js
  */
 mix.scripts([
     'jquery.js',
     'bootstrap.js',

 ]);

});
